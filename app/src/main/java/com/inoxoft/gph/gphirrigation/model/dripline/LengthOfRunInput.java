package com.inoxoft.gph.gphirrigation.model.dripline;

/**
 * Created by ie-07 on 2/17/17.
 */

public class LengthOfRunInput {

    private String mFlowRate;
    private String mNumberOfEmitters;
    private SizeOfHose mSizeOfHoseIPS;
    private SizeOfHose mSizeOfHosePE;

    public String getFlowRate() {
        return mFlowRate;
    }

    public void setFlowRate(String flowRate) {
        mFlowRate = flowRate;
    }

    public String getNumberOfEmitters() {
        return mNumberOfEmitters;
    }

    public void setNumberOfEmitters(String numberOfEmitters) {
        mNumberOfEmitters = numberOfEmitters;
    }

    public SizeOfHose getSizeOfHoseIPS() {
        return mSizeOfHoseIPS;
    }

    public void setSizeOfHoseIPS(SizeOfHose sizeOfHoseIPS) {
        mSizeOfHoseIPS = sizeOfHoseIPS;
    }

    public SizeOfHose getSizeOfHosePE() {
        return mSizeOfHosePE;
    }

    public void setSizeOfHosePE(SizeOfHose sizeOfHosePE) {
        mSizeOfHosePE = sizeOfHosePE;
    }
}
