package com.inoxoft.gph.gphirrigation.model.dripline;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ie-07 on 2/17/17.
 */

public class DriplineInput implements Parcelable {

    private String mAreaType;
    private SoilType mSoilType;
    private String mArea;
    private DriplinePSI mPSI;
    private boolean mIsSlopedInstallation;

    public String getAreaType() {
        return mAreaType;
    }

    public void setAreaType(String areaType) {
        mAreaType = areaType;
    }

    public SoilType getSoilType() {
        return mSoilType;
    }

    public void setSoilType(SoilType soilType) {
        mSoilType = soilType;
    }

    public String getArea() {
        return mArea;
    }

    public void setArea(String area) {
        mArea = area;
    }

    public DriplinePSI getPSI() {
        return mPSI;
    }

    public void setPSI(DriplinePSI psi) {
        mPSI = psi;
    }

    public boolean getIsmIsSlopedInstallation() {
        return mIsSlopedInstallation;
    }

    public void setIsSlopedInstallation(boolean isSlopedInstallation) {
        mIsSlopedInstallation = isSlopedInstallation;
    }

    //region Parcelable
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mAreaType);
        dest.writeParcelable(mSoilType, flags);
        dest.writeString(mArea);
        dest.writeParcelable(mPSI, flags);
        dest.writeByte(mIsSlopedInstallation ? (byte) 1 : (byte) 0);
    }

    public DriplineInput() {
    }

    protected DriplineInput(Parcel in) {
        mAreaType = in.readString();
        mSoilType = in.readParcelable(SoilType.class.getClassLoader());
        mArea = in.readString();
        mPSI = in.readParcelable(DriplinePSI.class.getClassLoader());
        mIsSlopedInstallation = in.readByte() != 0;
    }

    public static final Parcelable.Creator<DriplineInput> CREATOR = new Parcelable.Creator<DriplineInput>() {
        @Override
        public DriplineInput createFromParcel(Parcel source) {
            return new DriplineInput(source);
        }

        @Override
        public DriplineInput[] newArray(int size) {
            return new DriplineInput[size];
        }
    };
    //endregion
}
