package com.inoxoft.gph.gphirrigation.view.fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.inoxoft.gph.gphirrigation.R;
import com.inoxoft.gph.gphirrigation.view.core.BaseFragment;
import com.inoxoft.gph.gphirrigation.view.listeners.MainActivityComponentProvider;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by roman on 2/13/17.
 */

public class ContactsFragment extends BaseFragment {

    @BindView(R.id.resources_toolbar)
    Toolbar mToolbar;
    @BindView(R.id.f_contacts_map_view)
    MapView mMapView;

    private static final int GOOGLE_MAP_CAMERA_ZOOM = 16;
    private static final double LATITUDE = 34.107864;
    private static final double LONGITUDE = -117.483788;

    private MainActivityComponentProvider mMainActivityComponentProvider;

    public static ContactsFragment newInstance() {
        Bundle args = new Bundle();
        ContactsFragment fragment = new ContactsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    //region BaseFragment
    @Override
    protected int getContentViewID() {
        return R.layout.fragment_contacts;
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        ButterKnife.bind(this, view);
        initToolbar();
        mMainActivityComponentProvider.setStatusBarColor(R.color.colorPrimaryDark);
        mMapView.onCreate(savedInstanceState);
        initMapView();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivityComponentProvider) {
            mMainActivityComponentProvider = (MainActivityComponentProvider) activity;
        } else {
            throw new ClassCastException(activity.toString() + " must implement MainActivityComponentProvider");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        mMapView.onPause();
        super.onPause();
    }

    @Override
    public void onStop() {
        mMapView.onStop();
        super.onStop();
    }

    @Override
    public void onDestroy() {
        mMapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            getActivity().onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }
    //endregion

    //region Utility API
    @OnClick(R.id.f_contact_call_btn)
    void onCallButtonClick() {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(getResources().getString(R.string.phone_number)));
        startActivity(intent);
    }

    @OnClick(R.id.f_contact_directions_btn)
    void onDirectionsButtonClick() {
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?daddr=" + LATITUDE + "," + LONGITUDE));
        startActivity(intent);
    }

    @OnClick(R.id.f_contact_message_btn)
    void onMessageButtonClick() {
        sendEmail();
    }

    private void initMapView() {
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                LatLng sydney = new LatLng(LATITUDE, LONGITUDE); //coordinates from maps google

                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, GOOGLE_MAP_CAMERA_ZOOM));
                googleMap.addMarker(new MarkerOptions()
                        .position(sydney));
                googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                    @Override
                    public View getInfoWindow(Marker marker) {
                        return null;
                    }

                    @Override
                    public View getInfoContents(Marker marker) {
                        View view = LayoutInflater.from(getContext()).inflate(R.layout.map_marker_info_window, null);
                        TextView snippet = (TextView) view.findViewById(R.id.map_marker_snippet);
                        TextView title = (TextView) view.findViewById(R.id.map_marker_title);
                        snippet.setText(getResources().getString(R.string.gph_irrigation_address));
                        title.setText(getResources().getString(R.string.gph_irrigation_inc));
                        return view;
                    }
                });
            }
        });
    }

    private void initToolbar() {
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    private void sendEmail() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{getResources().getString(R.string.gph_irrigation_email_address)});
        intent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.email_subject));
        startActivity(intent);
    }
    //endregion
}
