package com.inoxoft.gph.gphirrigation.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.inoxoft.gph.gphirrigation.R;
import com.inoxoft.gph.gphirrigation.view.activity.AuditCalculatorActivity;
import com.inoxoft.gph.gphirrigation.view.activity.DripCalculatorActivity;
import com.inoxoft.gph.gphirrigation.view.adapter.MainScreenAdapter;
import com.inoxoft.gph.gphirrigation.view.core.BaseFragment;
import com.inoxoft.gph.gphirrigation.view.listeners.MainActivityComponentProvider;
import com.inoxoft.gph.gphirrigation.view.utils.MainScreenItemDecoration;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by roman on 2/13/17.
 */

public class MainFragment extends BaseFragment implements MainScreenAdapter.OnItemClickListener {

    @BindView(R.id.f_main_recycler_view)
    RecyclerView mRecyclerView;

    public static MainFragment newInstance() {
        Bundle args = new Bundle();
        MainFragment fragment = new MainFragment();
        fragment.setArguments(args);
        return fragment;
    }

    //region BaseFragment
    @Override
    protected int getContentViewID() {
        return R.layout.fragment_main;
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        ButterKnife.bind(this, view);
        initRecyclerView();
    }

    @Override
    public void onPause() {
        ((MainActivityComponentProvider) mComponentCommunicator).setButtonsWasAnimated(true);
        super.onPause();
    }
    //endregion

    //region MainScreenAdapter.OnItemClickListener
    @Override
    public void onItemClick(int position, int titleRes) {
        switch (position) {
            case 0:
                startActivity(new Intent(getContext(), DripCalculatorActivity.class));
                getActivity().overridePendingTransition(R.anim.fragment_enter_from_right, 0);
                break;
            case 1:
                startActivity(new Intent(getContext(), AuditCalculatorActivity.class));
                getActivity().overridePendingTransition(R.anim.fragment_enter_from_right, 0);
                break;
            case 2:
                mComponentCommunicator.navigateToFragment(ResourcesFragment.newInstance());
                break;
            case 3:
                mComponentCommunicator.navigateToFragment(ContactsFragment.newInstance());
                break;
        }

    }
    //endregion

    //region Utility API
    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addItemDecoration(new MainScreenItemDecoration(getResources().getDimensionPixelSize(R.dimen.padding_default),
                getResources().getDimensionPixelSize(R.dimen.padding_small)));
        MainScreenAdapter mainScreenAdapter = new MainScreenAdapter(this);
        mainScreenAdapter.setWasAnimated(((MainActivityComponentProvider) mComponentCommunicator).getButtonsWasAnimated());
        mRecyclerView.setAdapter(mainScreenAdapter);
    }
    //endregion
}
