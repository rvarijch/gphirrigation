package com.inoxoft.gph.gphirrigation.view.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.inoxoft.gph.gphirrigation.R;

/**
 * Created by roman on 2/15/17.
 */

public class InfoDialog {

    public static void showInfoDialog(Context context, int stringRes) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_info_view, null);
        TextView dialogContentText = (TextView) view.findViewById(R.id.dialog_text);
        dialogContentText.setText(stringRes);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(true)
                .setView(view)
                .setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }
}
