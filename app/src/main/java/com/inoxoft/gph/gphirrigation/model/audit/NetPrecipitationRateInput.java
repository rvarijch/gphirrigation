package com.inoxoft.gph.gphirrigation.model.audit;

/**
 * Created by ie-07 on 2/17/17.
 */

public class NetPrecipitationRateInput {

    private String mTotalVolume;
    private String mTotalCatchDevices;
    private String mTestRunTime;
    private String mCatchDevicesArea;

    public String getTotalVolume() {
        return mTotalVolume;
    }

    public void setTotalVolume(String totalVolume) {
        mTotalVolume = totalVolume;
    }

    public String getTotalCatchDevices() {
        return mTotalCatchDevices;
    }

    public void setTotalCatchDevices(String totalCatchDevices) {
        mTotalCatchDevices = totalCatchDevices;
    }

    public String getTestRunTime() {
        return mTestRunTime;
    }

    public void setTestRunTime(String testRunTime) {
        mTestRunTime = testRunTime;
    }

    public String getCatchDevicesArea() {
        return mCatchDevicesArea;
    }

    public void setCatchDevicesArea(String catchDevicesArea) {
        mCatchDevicesArea = catchDevicesArea;
    }
}
