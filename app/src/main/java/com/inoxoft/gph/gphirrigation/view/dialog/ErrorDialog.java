package com.inoxoft.gph.gphirrigation.view.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.inoxoft.gph.gphirrigation.R;

/**
 * Created by ie-07 on 2/21/17.
 */

public class ErrorDialog {

    public static void showInfoDialog(Context context, int stringRes, DialogInterface.OnClickListener listener) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_info_view, null);
        TextView dialogContentText = (TextView) view.findViewById(R.id.dialog_text);
        dialogContentText.setText(stringRes);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false)
                .setView(view)
                .setPositiveButton(context.getString(R.string.ok), listener).show();
    }
}
