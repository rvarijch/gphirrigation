package com.inoxoft.gph.gphirrigation.view.core;

import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.inoxoft.gph.gphirrigation.R;

/**
 * Created by 001 on 9/15/2016.
 */
public class BaseActivity extends AppCompatActivity implements android.os.Handler.Callback {

    private static final int MSG_SHOW_LOADING_DIALOG = 0x1000;
    private static final int MSG_UPDATE_LOADING_MESSAGE = 0x1001;
    private static final int MSG_HIDE_LOADING_DIALOG = 0x1002;

    protected final android.os.Handler mUIHandler = new android.os.Handler(BaseActivity.this);
    private ProgressDialog mProgressDialog;

    //region AppCompatActivity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    //endregion

    //region Handler.Callback
    @Override
    public boolean handleMessage(Message message) {
        boolean result = false;
        switch (message.what) {
            case MSG_SHOW_LOADING_DIALOG:
                if (null == mProgressDialog) {
                    mProgressDialog = new ProgressDialog(BaseActivity.this);
                }
                mProgressDialog.setMessage((String) message.obj);
                if (!mProgressDialog.isShowing()) {
                    mProgressDialog.show();
                }
                result = true;
                break;
            case MSG_UPDATE_LOADING_MESSAGE:
                if (null != mProgressDialog) {
                    mProgressDialog.setMessage((String) message.obj);
                }
                result = true;
                break;
            case MSG_HIDE_LOADING_DIALOG:
                if ((null != mProgressDialog) && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
                result = true;
                break;
        }
        return result;
    }
    //endregion

    //region BaseActivity

    /**
     * Adds a {@link Fragment} to this activity's layout.
     *
     * @param containerViewId The container view to where add the fragment.
     * @param fragment        The fragment to be added.
     */
    protected void addFragment(int containerViewId, Fragment fragment, boolean addToBackStack) {
        FragmentTransaction fragmentTransaction = this.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(containerViewId, fragment);
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(fragment.getClass().getName());
        }
        fragmentTransaction.commit();
    }

    protected void addFragment(int containerViewId, Fragment fragment, String tag, boolean addToBackStack) {
        FragmentTransaction fragmentTransaction = this.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(containerViewId, fragment, tag);
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(fragment.getClass().getName());
        }
        fragmentTransaction.commit();
    }

    public void replaceFragment(int containerViewId, Fragment fragment, boolean addToBackStack, boolean animateTransaction) {
        FragmentTransaction fragmentTransaction = this.getSupportFragmentManager().beginTransaction();
        if (animateTransaction && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            fragmentTransaction.setCustomAnimations(R.anim.fragment_enter_from_right, 0,
                    0, R.anim.fragment_exit_to_right);
        }
        fragmentTransaction.replace(containerViewId, fragment);
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(fragment.getClass().getName());
        }
        fragmentTransaction.commit();
    }

    public void removeFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.remove(fragment);
        transaction.commit();
    }

    public void removeFragment(String tag) {
        FragmentTransaction fragmentTransaction = this.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.remove(this.getSupportFragmentManager().findFragmentByTag(tag)).commit();
    }

    /**
     * Shows loading dialog.
     *
     * @param messageValue message value
     */
    public void showLoadingDialog(String messageValue) {
        Message message = new Message();
        message.what = MSG_SHOW_LOADING_DIALOG;
        message.obj = messageValue;
        mUIHandler.sendMessage(message);
    }

    /**
     * Updates loading dialog message.
     *
     * @param messageValue message value
     */
    public void updateLoadingDialogMessage(String messageValue) {
        Message message = new Message();
        message.what = MSG_UPDATE_LOADING_MESSAGE;
        message.obj = messageValue;
        mUIHandler.sendMessage(message);
    }

    /**
     * Hides loading dialog.
     */
    public void hideLoadingDialog() {
        Message message = new Message();
        message.what = MSG_HIDE_LOADING_DIALOG;
        mUIHandler.sendMessage(message);
    }
    //endregion
}
