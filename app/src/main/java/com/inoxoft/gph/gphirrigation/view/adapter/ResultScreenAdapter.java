package com.inoxoft.gph.gphirrigation.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.inoxoft.gph.gphirrigation.R;
import com.inoxoft.gph.gphirrigation.calculator.Calculator;
import com.inoxoft.gph.gphirrigation.model.dripline.DriplineResult;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ie-07 on 2/20/17.
 */

public class ResultScreenAdapter extends RecyclerView.Adapter<ResultScreenAdapter.ViewHolder> {

    private List<DriplineResult> mDataList;
    private Context mContext;

    public ResultScreenAdapter(Context context) {
        mContext = context;
        mDataList = new ArrayList<>();
    }

    //region RecyclerView.Adapter
    @Override
    public ResultScreenAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dripline_result, parent, false);
        return new ResultScreenAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        DriplineResult item = mDataList.get(position);
        if (item.getProductType().equals("")) {
            holder.productTypeLabel.setVisibility(View.GONE);
            holder.productType.setVisibility(View.GONE);
        } else
            holder.productType.setText(item.getProductType());
        holder.partNumber.setText(item.getPartNumber());
        holder.emitterFlow.setText(item.getEmitterFlow());
        holder.emitterSpacing.setText(String.valueOf(Calculator.EMITTER_SPASING));
        holder.totalDripZoneFlow.setText(item.getTotalDripZoneFlow());
        holder.maxLengthOfTube.setText(item.getMaxLateralLengthOfTube());
        holder.totalLengthOfZoneDripline.setText(item.getTotalLengthOfZoneDripline());
        holder.applicationRate.setText(item.getApplicationRate());
        holder.timeToApplyQuartOfWater.setText(item.getTimeToApplyQuarterOfWater());
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    public void addData(List<DriplineResult> dataList) {
        mDataList.addAll(dataList);
        notifyDataSetChanged();
    }
    //endregion

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView productTypeLabel;
        TextView productType;
        TextView partNumber;
        TextView emitterFlow;
        TextView emitterSpacing;
        TextView totalDripZoneFlow;
        TextView maxLengthOfTube;
        TextView totalLengthOfZoneDripline;
        TextView applicationRate;
        TextView timeToApplyQuartOfWater;

        private ViewHolder(View v) {
            super(v);
            productTypeLabel = (TextView) v.findViewById(R.id.product_type_label);
            productType = (TextView) v.findViewById(R.id.f_dripline_result_product_type_value);
            partNumber = (TextView) v.findViewById(R.id.f_dripline_result_part_number_value);
            emitterFlow = (TextView) v.findViewById(R.id.f_dripline_result_emitter_flow_value);
            emitterSpacing = (TextView) v.findViewById(R.id.f_dripline_result_emitter_spacing_value);
            totalDripZoneFlow = (TextView) v.findViewById(R.id.f_dripline_result_total_drip_zone_flow_value);
            maxLengthOfTube = (TextView) v.findViewById(R.id.f_dripline_result_max_lateral_length_of_tube_value);
            totalLengthOfZoneDripline = (TextView) v.findViewById(R.id.f_dripline_result_total_length_of_zone_dripline_value);
            applicationRate = (TextView) v.findViewById(R.id.f_dripline_result_application_rate_value);
            timeToApplyQuartOfWater = (TextView) v.findViewById(R.id.f_dripline_result_time_to_aplly_value);
        }
    }
}
