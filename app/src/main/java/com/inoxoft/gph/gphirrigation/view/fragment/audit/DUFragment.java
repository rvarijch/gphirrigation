package com.inoxoft.gph.gphirrigation.view.fragment.audit;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.inoxoft.gph.gphirrigation.R;
import com.inoxoft.gph.gphirrigation.calculator.Calculator;
import com.inoxoft.gph.gphirrigation.view.core.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by roman on 2/13/17.
 */

public class DUFragment extends BaseFragment {

    @BindView(R.id.f_du_average_low_quarter_input)
    EditText mLowAverageQuarterInputField;
    @BindView(R.id.f_du_number_of_catch_devices_input)
    EditText mNumberOfCatchDevicesInputField;
    @BindView(R.id.f_du_total_from_catch_devices_input)
    EditText mTotalFromCatchDevicesInputField;
    @BindView(R.id.f_du_plant_water_requirement_input)
    EditText mPlantWaterRequirementInputField;
    @BindView(R.id.f_du_water_requirement_result_units)
    TextView mWaterRequirementUnitsLabel;
    @BindView(R.id.f_du_distribution_uniformity_result)
    TextView mDistributionUniformityResult;
    @BindView(R.id.f_du_water_requirement_result)
    TextView mWaterRequirementResult;

    public static DUFragment newInstance() {
        Bundle args = new Bundle();
        DUFragment fragment = new DUFragment();
        fragment.setArguments(args);
        return fragment;
    }

    //region BaseFragment
    @Override
    protected int getContentViewID() {
        return R.layout.fragment_du;
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        ButterKnife.bind(this, view);
        if (mLowAverageQuarterInputField.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
        initDistributionUniformityInputs();
    }
    //endregion

    //region Utility API
    private void initDistributionUniformityInputs() {
        mLowAverageQuarterInputField.addTextChangedListener(mDistributionUniformity);
        mNumberOfCatchDevicesInputField.addTextChangedListener(mDistributionUniformity);
        mTotalFromCatchDevicesInputField.addTextChangedListener(mDistributionUniformity);

        mPlantWaterRequirementInputField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (checkFieldsForWaterRequirement()) {
                    if (!checkInputsOnNumericPoint() && !checkInputsOnZeroValues()) {
                        double distributionUniformity = Calculator.calculateDistributionUniformity(
                                mLowAverageQuarterInputField.getText().toString(),
                                mNumberOfCatchDevicesInputField.getText().toString(),
                                mTotalFromCatchDevicesInputField.getText().toString());

                        mWaterRequirementUnitsLabel.setVisibility(View.VISIBLE);
                        mWaterRequirementResult.setText(Calculator.calculateWaterRequirement(mPlantWaterRequirementInputField.getText().toString(), distributionUniformity));
                    } else {
                        mWaterRequirementResult.setText("0");
                        mWaterRequirementUnitsLabel.setVisibility(View.INVISIBLE);
                    }
                } else {
                    mWaterRequirementResult.setText("0");
                    mWaterRequirementUnitsLabel.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    private TextWatcher mDistributionUniformity = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (checkFieldsForDistributionUniformityIsEmpty()) {
                if (!checkDistributionUniformityInputOnNumericPoint() && !checkDistributionUniformityInputOnZeroValue()) {
                    double distributionUniformity = Calculator.calculateDistributionUniformity(
                            mLowAverageQuarterInputField.getText().toString(),
                            mNumberOfCatchDevicesInputField.getText().toString(),
                            mTotalFromCatchDevicesInputField.getText().toString());

                    mDistributionUniformityResult.setText(String.valueOf(distributionUniformity) + " %");

                    if (!TextUtils.isEmpty(mPlantWaterRequirementInputField.getText().toString())) {
                        mWaterRequirementUnitsLabel.setVisibility(View.VISIBLE);
                        mWaterRequirementResult.setText(Calculator.calculateWaterRequirement(mPlantWaterRequirementInputField.getText().toString(), distributionUniformity));
                    }
                } else {
                    mDistributionUniformityResult.setText("0");
                }
            } else {
                mDistributionUniformityResult.setText("0");
                mWaterRequirementResult.setText("0");
                mWaterRequirementUnitsLabel.setVisibility(View.INVISIBLE);
            }
        }
    };

    private boolean checkInputsOnZeroValues() {
        return Double.valueOf(mPlantWaterRequirementInputField.getText().toString()) == 0d || checkDistributionUniformityInputOnZeroValue();
    }

    private boolean checkDistributionUniformityInputOnZeroValue() {
        return Double.valueOf(mLowAverageQuarterInputField.getText().toString()) == 0d ||
                Double.valueOf(mNumberOfCatchDevicesInputField.getText().toString()) == 0d ||
                Double.valueOf(mTotalFromCatchDevicesInputField.getText().toString()) == 0d;
    }

    private boolean checkInputsOnNumericPoint() {
        return mPlantWaterRequirementInputField.getText().toString().equals(".") || checkDistributionUniformityInputOnNumericPoint();
    }

    private boolean checkDistributionUniformityInputOnNumericPoint() {
        return (mLowAverageQuarterInputField.getText().toString().equals(".") ||
                mNumberOfCatchDevicesInputField.getText().toString().equals(".") ||
                mTotalFromCatchDevicesInputField.getText().toString().equals("."));
    }

    private boolean checkFieldsForWaterRequirement() {
        return checkFieldsForDistributionUniformityIsEmpty() &&
                !TextUtils.isEmpty(mPlantWaterRequirementInputField.getText().toString());
    }

    private boolean checkFieldsForDistributionUniformityIsEmpty() {
        return !TextUtils.isEmpty(mLowAverageQuarterInputField.getText().toString()) &&
                !TextUtils.isEmpty(mNumberOfCatchDevicesInputField.getText().toString()) &&
                !TextUtils.isEmpty(mTotalFromCatchDevicesInputField.getText().toString());
    }
    //endregion
}
