package com.inoxoft.gph.gphirrigation.view.listeners;

/**
 * Created by ie-07 on 2/16/17.
 */

public interface MainActivityComponentProvider extends ComponentCommunicator {

    void setStatusBarColor(int colorRes);

    void setButtonsWasAnimated(boolean buttonsWasAnimated);

    boolean getButtonsWasAnimated();
}
