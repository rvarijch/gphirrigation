package com.inoxoft.gph.gphirrigation.view.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.inoxoft.gph.gphirrigation.Constants;
import com.inoxoft.gph.gphirrigation.R;
import com.inoxoft.gph.gphirrigation.view.adapter.DripTabsAdapter;
import com.inoxoft.gph.gphirrigation.view.core.BaseActivity;
import com.inoxoft.gph.gphirrigation.view.dialog.InfoDialog;
import com.inoxoft.gph.gphirrigation.view.listeners.ComponentCommunicator;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by roman on 2/13/17.
 */

public class DripCalculatorActivity extends BaseActivity implements ComponentCommunicator {

    @BindView(R.id.activity_drip_tab_layout_view_pager)
    ViewPager mDripViewPager;
    @BindView(R.id.activity_drip_tab_layout)
    TabLayout mDripTabLayout;
    @BindView(R.id.activity_drip_toolbar)
    Toolbar mToolbar;
    @BindView(R.id.activity_drip_app_bar)
    AppBarLayout mAppBar;

    //region BaseActivity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drip_calculator);
        ButterKnife.bind(this);

        initToolbar();
        initTabs();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_drip, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        } else if (menuItem.getItemId() == R.id.menu_drip_info) {
            InfoDialog.showInfoDialog(this, R.string.drip_info_text);
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onBackPressed() {
        mDripTabLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                mDripTabLayout.setVisibility(View.VISIBLE);
            }
        }, Constants.BASE_ANIMATION_DURATION);
        super.onBackPressed();
        overridePendingTransition(0, R.anim.fragment_exit_to_right);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }
    //endregion

    //region ComponentCommunicator
    @Override
    public void navigateToFragment(final Fragment fragment) {
        navigateToFragmentWithAppBarAnimation(fragment);
    }
    //endregion

    //region Utility API
    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    private void initTabs() {
        mDripTabLayout.addTab(mDripTabLayout.newTab().setCustomView(initTabView(R.string.dripline)));
        mDripTabLayout.addTab(mDripTabLayout.newTab().setCustomView(initTabView(R.string.length_of_run)));
        mDripTabLayout.addTab(mDripTabLayout.newTab().setCustomView(initTabView(R.string.max_emitters)));
        mDripTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        mDripTabLayout.setTabMode(TabLayout.MODE_FIXED);

        mDripTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mDripViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        DripTabsAdapter adapter = new DripTabsAdapter(getSupportFragmentManager(), mDripTabLayout.getTabCount());
        mDripViewPager.setAdapter(adapter);
        mDripViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mDripTabLayout));
    }

    private View initTabView(int tabTitle) {
        View view = LayoutInflater.from(this).inflate(R.layout.item_tab_layout, null);
        TextView textView = (TextView) view.findViewById(R.id.tab_text);
        textView.setText(tabTitle);
        return view;
    }

    private void navigateToFragmentWithAppBarAnimation(final Fragment fragment) {
        mAppBar.animate()
                .translationY(-mAppBar.getHeight())
                .setDuration(Constants.BASE_ANIMATION_DURATION)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        mDripTabLayout.setVisibility(View.GONE);
                        animation.removeListener(this);
                    }
                });

        mAppBar.postDelayed(new Runnable() {
            @Override
            public void run() {
                mAppBar.animate()
                        .translationY(0)
                        .setDuration(Constants.BASE_ANIMATION_DURATION);
                replaceFragment(R.id.activity_drip_fragment_container, fragment, true, true);
            }
        }, Constants.BASE_ANIMATION_DURATION);
    }
    //endregion
}
