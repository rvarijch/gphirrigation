package com.inoxoft.gph.gphirrigation.view.utils;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by roman on 2/14/17.
 */

public class MainScreenItemDecoration extends RecyclerView.ItemDecoration {

    private final int mOutSpace;
    private final int mInnerSpace;

    public MainScreenItemDecoration(int outSpace, int innerSpace) {
        mOutSpace = outSpace;
        mInnerSpace = innerSpace;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {

        if (parent.getChildAdapterPosition(view) == 0) {
            outRect.set(mOutSpace, 0, mInnerSpace, mInnerSpace);
        }

        if (parent.getChildAdapterPosition(view) == 1) {
            outRect.set(mInnerSpace, 0, mOutSpace, mInnerSpace);
        }

        if (parent.getChildAdapterPosition(view) > 1) {
            if (parent.getChildAdapterPosition(view) % 2 != 0) {
                outRect.set(mInnerSpace, mInnerSpace, mOutSpace, mOutSpace);
            } else {
                outRect.set(mOutSpace, mInnerSpace, mInnerSpace, mOutSpace);
            }
        }
    }
}
