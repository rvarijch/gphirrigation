package com.inoxoft.gph.gphirrigation.model.dripline;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ie-07 on 2/20/17.
 */

public class DriplinePSI implements Parcelable {

    private String mTitle;
    private int mValueForOneGPH;
    private int mValueForHalfGPH;

    public DriplinePSI(String title, int valueForOneGPH, int valueForHalfGPH) {
        mTitle = title;
        mValueForOneGPH = valueForOneGPH;
        mValueForHalfGPH = valueForHalfGPH;
    }

    public String getTitle() {
        return mTitle;
    }

    public int getValueForOneGPH() {
        return mValueForOneGPH;
    }

    public int getValueForHalfGPH() {
        return mValueForHalfGPH;
    }

    //region Parcelable
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mTitle);
        dest.writeInt(mValueForOneGPH);
        dest.writeInt(mValueForHalfGPH);
    }

    protected DriplinePSI(Parcel in) {
        mTitle = in.readString();
        mValueForOneGPH = in.readInt();
        mValueForHalfGPH = in.readInt();
    }

    public static final Parcelable.Creator<DriplinePSI> CREATOR = new Parcelable.Creator<DriplinePSI>() {
        @Override
        public DriplinePSI createFromParcel(Parcel source) {
            return new DriplinePSI(source);
        }

        @Override
        public DriplinePSI[] newArray(int size) {
            return new DriplinePSI[size];
        }
    };
    //endregion
}
