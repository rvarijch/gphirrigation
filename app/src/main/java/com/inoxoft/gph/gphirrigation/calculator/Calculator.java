package com.inoxoft.gph.gphirrigation.calculator;

import com.inoxoft.gph.gphirrigation.model.audit.NetPrecipitationRateInput;
import com.inoxoft.gph.gphirrigation.model.dripline.DriplineInput;
import com.inoxoft.gph.gphirrigation.model.dripline.DriplinePSI;
import com.inoxoft.gph.gphirrigation.model.dripline.DriplineResult;
import com.inoxoft.gph.gphirrigation.model.dripline.LengthOfRunInput;
import com.inoxoft.gph.gphirrigation.model.dripline.MaxEmittersInput;

import java.math.BigDecimal;
import java.math.RoundingMode;


/**
 * Created by ie-07 on 2/17/17.
 */

public class Calculator {

    public static final int EMITTER_SPASING = 12;
    public static final double DRIPLINE_EMITTER_FLOW_ONE = 1;
    public static final double DRIPLINE_EMITTER_FLOW_HALF = 0.5;

    private static final String PRODUCT_TYPE_GHVY = "GHVY";
    private static final String PART_NUMBER_HALF_GPH = "0512100, 0512500";
    private static final String PART_NUMBER_ONE_GPH = "112100, 112500";

    //region Calculator
    public static DriplineResult calculateDriplineByGPM(DriplineInput driplineInput, double gpm) {
        double area = Double.valueOf(driplineInput.getArea());
        BigDecimal totalLengthOfZoneDripline = new BigDecimal(area);
        String totalLengthOfZoneDriplineString = totalLengthOfZoneDripline.setScale(2, BigDecimal.ROUND_HALF_UP).toString();

        double totalDripZoneFlow = (area * ((gpm / 60) * 10)) / 10;
        BigDecimal totalDripZoneFlowBD = new BigDecimal(totalDripZoneFlow);
        String totalDripZoneFlowString = totalDripZoneFlowBD.setScale(2, BigDecimal.ROUND_HALF_UP).toString();

        double applicationRate = ((totalDripZoneFlow / area) / 452.57) * 43560;
        BigDecimal applicationRateBD = new BigDecimal(applicationRate);
        String applicationRateString = applicationRateBD.setScale(2, BigDecimal.ROUND_HALF_UP).toString();

        double timeToApplyWater = (applicationRate * 0.25) * driplineInput.getSoilType().getValue() * 160;
        BigDecimal timeToApplyWaterBD = new BigDecimal(timeToApplyWater);
        String timeToApplyWaterString = timeToApplyWaterBD.setScale(0, BigDecimal.ROUND_HALF_UP).toString();

        DriplineResult result = new DriplineResult();

        if (gpm == DRIPLINE_EMITTER_FLOW_ONE) {
            result.setPartNumber(PART_NUMBER_ONE_GPH);
            result.setProductType(PRODUCT_TYPE_GHVY);
        } else if (gpm == DRIPLINE_EMITTER_FLOW_HALF) {
            result.setPartNumber(PART_NUMBER_HALF_GPH);
        }

        result.setEmitterFlow(String.valueOf(gpm));
        result.setTotalLengthOfZoneDripline(totalLengthOfZoneDriplineString);
        result.setApplicationRate(applicationRateString);
        result.setTotalDripZoneFlow(totalDripZoneFlowString);
        result.setMaxLateralLengthOfTube(String.valueOf(getLengthOfTube(gpm, driplineInput.getPSI())));
        result.setTimeToApplyQuarterOfWater(timeToApplyWaterString);
        return result;
    }

    public static String calculateLengthOfRun(LengthOfRunInput object, boolean isForIPS) {
        double actionA = Math.pow(((Double.valueOf(object.getFlowRate()) * Double.valueOf(object.getNumberOfEmitters())) / 60d), 1.852d) * 4.52d;
        double actionB = isForIPS ? 8222.87d * Math.pow(object.getSizeOfHoseIPS().getValue(), 4.87) :
                9432.55d * Math.pow(object.getSizeOfHosePE().getValue(), 4.87);
        double actionC = 30d / (actionA / actionB);

        BigDecimal bg = BigDecimal.valueOf(actionC);
        return bg.setScale(2, RoundingMode.HALF_UP).toString();
    }

    public static int calculateMaxEmitters(MaxEmittersInput object, boolean isForIPS) {
        double actionA = Double.valueOf(object.getLengthOfRun()) * 4.52d;

        double actionB = isForIPS ? 8222.87d * Math.pow(object.getSizeOfHoseIPS().getValue(), 4.87) :
                9432.5d * Math.pow(object.getSizeOfHosePE().getValue(), 4.87);

        double actionC = Math.pow((30d / (actionA / actionB)), 0.53995680345572d);

        BigDecimal bg = BigDecimal.valueOf(actionC * (60d / Double.valueOf(object.getFlowRate())));
        return bg.setScale(0, RoundingMode.HALF_UP).intValue();
    }

    public static double calculateGrossPrecipitationRate(String totalGPMString, String totalCatchAreaString) {
        double totalGPM = Double.valueOf(totalGPMString);
        double totalCatchArea = Double.valueOf(totalCatchAreaString);
        double constant = 96.25;
        double GPR = constant * totalGPM / totalCatchArea;
        BigDecimal bg = BigDecimal.valueOf(GPR);
        return bg.setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    public static double calculateNetPrecipitationRate(NetPrecipitationRateInput inputObject) {
        double totalVolume = Double.valueOf(inputObject.getTotalVolume());
        double totalCatchDevices = Double.valueOf(inputObject.getTotalCatchDevices());
        double testRunTime = Double.valueOf(inputObject.getTestRunTime());
        double catchDevicesArea = Double.valueOf(inputObject.getCatchDevicesArea());

        double vAvg = totalVolume / totalCatchDevices;
        double NPR = (3.66 * vAvg) / (testRunTime * catchDevicesArea);
        BigDecimal bg = BigDecimal.valueOf(NPR);
        return bg.setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    public static double calculateDistributionUniformity(String averageLowQuarter, String catchDevices, String totalFromCatchDevices) {
        double lowQuarter = Double.valueOf(averageLowQuarter) / 4d;
        double total = Double.valueOf(totalFromCatchDevices) / Double.valueOf(catchDevices);
        double res = 100 * lowQuarter / total;
        BigDecimal result = new BigDecimal(res);
        return result.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    public static String calculateWaterRequirement(String plantWaterRequirement, double distributionUniformity) {
        double waterRequirement = Double.valueOf(plantWaterRequirement) / (distributionUniformity / 100);
        BigDecimal result = new BigDecimal(waterRequirement);
        return result.setScale(2, BigDecimal.ROUND_HALF_UP).toString();
    }
    //endregion

    //UtilityAPI
    private static int getLengthOfTube(double gpm, DriplinePSI psi) {
        return gpm == DRIPLINE_EMITTER_FLOW_ONE ? psi.getValueForOneGPH() : psi.getValueForHalfGPH();
    }
    //endregion
}
