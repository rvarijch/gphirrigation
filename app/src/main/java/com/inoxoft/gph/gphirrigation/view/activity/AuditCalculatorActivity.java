package com.inoxoft.gph.gphirrigation.view.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.inoxoft.gph.gphirrigation.R;
import com.inoxoft.gph.gphirrigation.view.adapter.AuditTabsAdapter;
import com.inoxoft.gph.gphirrigation.view.core.BaseActivity;
import com.inoxoft.gph.gphirrigation.view.dialog.InfoDialog;
import com.inoxoft.gph.gphirrigation.view.listeners.ComponentCommunicator;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by roman on 2/13/17.
 */

public class AuditCalculatorActivity extends BaseActivity implements ComponentCommunicator {

    @BindView(R.id.activity_audit_tab_layout_view_pager)
    ViewPager mDripViewPager;
    @BindView(R.id.activity_audit_tab_layout)
    TabLayout mDripTabLayout;
    @BindView(R.id.activity_audit_toolbar)
    Toolbar mToolbar;

    //region BaseActivity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audit_calculator);
        ButterKnife.bind(this);

        initToolbar();
        initTabs();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_audit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        } else if (menuItem.getItemId() == R.id.menu_audit_info) {
            InfoDialog.showInfoDialog(this, R.string.audit_info_text);
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void navigateToFragment(Fragment fragment) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.fragment_exit_to_right);
    }
    //endregion

    //region Utility API
    private void initToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    private void initTabs() {
        mDripTabLayout.addTab(mDripTabLayout.newTab().setCustomView(initTabView(R.string.precipitation_rate)));
        mDripTabLayout.addTab(mDripTabLayout.newTab().setCustomView(initTabView(R.string.du)));
        mDripTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        mDripTabLayout.setTabMode(TabLayout.MODE_FIXED);
        mDripTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mDripViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        AuditTabsAdapter adapter = new AuditTabsAdapter(getSupportFragmentManager(), mDripTabLayout.getTabCount());
        mDripViewPager.setAdapter(adapter);
        mDripViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mDripTabLayout));
    }

    private View initTabView(int tabTitle) {
        View view = LayoutInflater.from(this).inflate(R.layout.item_tab_layout, null);
        TextView textView = (TextView) view.findViewById(R.id.tab_text);
        textView.setText(tabTitle);
        return view;
    }
    //endregion
}
