package com.inoxoft.gph.gphirrigation.view.fragment.audit;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.inoxoft.gph.gphirrigation.R;
import com.inoxoft.gph.gphirrigation.calculator.Calculator;
import com.inoxoft.gph.gphirrigation.model.audit.NetPrecipitationRateInput;
import com.inoxoft.gph.gphirrigation.view.core.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by roman on 2/13/17.
 */

public class PrecipitationRateFragment extends BaseFragment {

    @BindView(R.id.f_precipitation_rate_total_gpm_input)
    EditText mTotalGPMInputField;
    @BindView(R.id.f_precipitation_rate_total_catch_area_input)
    EditText mTotalCatchAreaInputField;
    @BindView(R.id.f_precipitation_gross_precipitation_rate_result)
    TextView mGrossPrecipitationRateResult;
    @BindView(R.id.f_precipitation_rate_total_volume_input)
    EditText mTotalVolumeInputField;
    @BindView(R.id.f_precipitation_rate_total_catch_devices_input)
    EditText mTotalCatchDevicesInputField;
    @BindView(R.id.f_precipitation_rate_test_run_time_input)
    EditText mTestRunTimeInputField;
    @BindView(R.id.f_precipitation_rate_catch_devices_area_input)
    EditText mCatchDevicesAreaInputField;
    @BindView(R.id.f_precipitation_net_precipitation_rate_result)
    TextView mNetPrecipitationRateResult;

    private NetPrecipitationRateInput mNPRInput;

    public static PrecipitationRateFragment newInstance() {
        Bundle args = new Bundle();
        PrecipitationRateFragment fragment = new PrecipitationRateFragment();
        fragment.setArguments(args);
        return fragment;
    }

    //region BaseFragment
    @Override
    protected int getContentViewID() {
        return R.layout.fragment_precipitation_rate;
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        ButterKnife.bind(this, view);
        if (mTotalGPMInputField.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
        mNPRInput = new NetPrecipitationRateInput();
        setTextWatchersForAllInputFields();
    }
    //endregion

    //region Utility API
    private void setTextWatchersForAllInputFields() {
        GPRTextWatcher gprTextWatcher = new GPRTextWatcher();
        mTotalGPMInputField.addTextChangedListener(gprTextWatcher);
        mTotalCatchAreaInputField.addTextChangedListener(gprTextWatcher);

        NPRTextWatcher nprTextWatcher = new NPRTextWatcher();
        mTotalVolumeInputField.addTextChangedListener(nprTextWatcher);
        mTotalCatchDevicesInputField.addTextChangedListener(nprTextWatcher);
        mTestRunTimeInputField.addTextChangedListener(nprTextWatcher);
        mCatchDevicesAreaInputField.addTextChangedListener(nprTextWatcher);
    }

    private NetPrecipitationRateInput configInput() {
        mNPRInput.setCatchDevicesArea(mCatchDevicesAreaInputField.getText().toString());
        mNPRInput.setTestRunTime(mTestRunTimeInputField.getText().toString());
        mNPRInput.setTotalVolume(mTotalVolumeInputField.getText().toString());
        mNPRInput.setTotalCatchDevices(mTotalCatchDevicesInputField.getText().toString());
        return mNPRInput;
    }

    private class GPRTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (!isGPRFieldsEmpty()) {
                if (!checkGPRInputsOnNumericPoint() && !checkGPRInputsOnZeroValues()) {
                    double result = Calculator.calculateGrossPrecipitationRate(mTotalGPMInputField.getText().toString(), mTotalCatchAreaInputField.getText().toString());
                    mGrossPrecipitationRateResult.setText(String.valueOf(result) + " " + getString(R.string.in_hour));
                }
            } else {
                mGrossPrecipitationRateResult.setText("0");
            }
        }
    }

    private class NPRTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (!isNPRFieldsEmpty()) {
                if (!checkNPRInputsOnNumericPoint() && !checkNPRInputsOnZeroValues()) {
                    double result = Calculator.calculateNetPrecipitationRate(configInput());
                    mNetPrecipitationRateResult.setText(String.valueOf(result) + " " + getString(R.string.in_hour));
                }
            } else {
                mNetPrecipitationRateResult.setText("0");
            }
        }
    }

    private boolean isGPRFieldsEmpty() {
        return TextUtils.isEmpty(mTotalGPMInputField.getText().toString())
                || TextUtils.isEmpty(mTotalCatchAreaInputField.getText().toString());
    }

    private boolean checkGPRInputsOnZeroValues() {
        return Double.valueOf(mTotalCatchAreaInputField.getText().toString()) == 0;
    }

    private boolean checkGPRInputsOnNumericPoint() {
        return mTotalGPMInputField.getText().toString().equals(".") || mTotalCatchAreaInputField.getText().toString().equals(".");
    }

    private boolean isNPRFieldsEmpty() {
        return (TextUtils.isEmpty(mTotalVolumeInputField.getText().toString())
                || TextUtils.isEmpty(mTotalCatchDevicesInputField.getText().toString())
                || TextUtils.isEmpty(mTestRunTimeInputField.getText().toString())
                || TextUtils.isEmpty(mCatchDevicesAreaInputField.getText().toString()));
    }

    private boolean checkNPRInputsOnZeroValues() {
        return Double.valueOf(mTestRunTimeInputField.getText().toString()) == 0 ||
                Double.valueOf(mTotalCatchDevicesInputField.getText().toString()) == 0 ||
                Double.valueOf(mCatchDevicesAreaInputField.getText().toString()) == 0;
    }

    private boolean checkNPRInputsOnNumericPoint() {
        return mTotalVolumeInputField.getText().toString().equals(".") || mTotalCatchDevicesInputField.getText().toString().equals(".") ||
                mTestRunTimeInputField.getText().toString().equals(".") || mCatchDevicesAreaInputField.getText().toString().equals(".");
    }
    //endregion
}
