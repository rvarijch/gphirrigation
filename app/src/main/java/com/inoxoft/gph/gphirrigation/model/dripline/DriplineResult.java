package com.inoxoft.gph.gphirrigation.model.dripline;

/**
 * Created by ie-07 on 2/17/17.
 */

public class DriplineResult {

    private String mProductType;
    private String mPartNumber;
    private String mEmitterFlow;
    private String mTotalDripZoneFlow;
    private String mMaxLateralLengthOfTube;
    private String mTotalLengthOfZoneDripline;
    private String mApplicationRate;
    private String mTimeToApplyQuarterOfWater;

    public DriplineResult() {
        this("", "", "", "", "", "", "", "");
    }

    public DriplineResult(String productType, String partNumber, String emitterFlow, String totalDripZoneFlow, String maxLateralLengthOfTube,
                          String totalLengthOfZoneDripline, String applicationRate, String timeToApplyQuarterOfWater) {
        mProductType = productType;
        mPartNumber = partNumber;
        mEmitterFlow = emitterFlow;
        mTotalDripZoneFlow = totalDripZoneFlow;
        mMaxLateralLengthOfTube = maxLateralLengthOfTube;
        mTotalLengthOfZoneDripline = totalLengthOfZoneDripline;
        mApplicationRate = applicationRate;
        mTimeToApplyQuarterOfWater = timeToApplyQuarterOfWater;
    }

    public String getProductType() {
        return mProductType;
    }

    public void setProductType(String productType) {
        mProductType = productType;
    }

    public String getPartNumber() {
        return mPartNumber;
    }

    public void setPartNumber(String partNumber) {
        mPartNumber = partNumber;
    }

    public String getEmitterFlow() {
        return mEmitterFlow;
    }

    public void setEmitterFlow(String emitterFlow) {
        mEmitterFlow = emitterFlow;
    }

    public String getTotalDripZoneFlow() {
        return mTotalDripZoneFlow;
    }

    public void setTotalDripZoneFlow(String totalDripZoneFlow) {
        mTotalDripZoneFlow = totalDripZoneFlow;
    }

    public String getMaxLateralLengthOfTube() {
        return mMaxLateralLengthOfTube;
    }

    public void setMaxLateralLengthOfTube(String maxLateralLengthOfTube) {
        mMaxLateralLengthOfTube = maxLateralLengthOfTube;
    }

    public String getTotalLengthOfZoneDripline() {
        return mTotalLengthOfZoneDripline;
    }

    public void setTotalLengthOfZoneDripline(String totalLengthOfZoneDripline) {
        mTotalLengthOfZoneDripline = totalLengthOfZoneDripline;
    }

    public String getApplicationRate() {
        return mApplicationRate;
    }

    public void setApplicationRate(String applicationRate) {
        mApplicationRate = applicationRate;
    }

    public String getTimeToApplyQuarterOfWater() {
        return mTimeToApplyQuarterOfWater;
    }

    public void setTimeToApplyQuarterOfWater(String timeToApplyQuarterOfWater) {
        mTimeToApplyQuarterOfWater = timeToApplyQuarterOfWater;
    }
}
