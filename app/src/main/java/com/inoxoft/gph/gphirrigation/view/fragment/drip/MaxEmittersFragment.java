package com.inoxoft.gph.gphirrigation.view.fragment.drip;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.inoxoft.gph.gphirrigation.R;
import com.inoxoft.gph.gphirrigation.calculator.Calculator;
import com.inoxoft.gph.gphirrigation.model.dripline.MaxEmittersInput;
import com.inoxoft.gph.gphirrigation.model.dripline.SizeOfHose;
import com.inoxoft.gph.gphirrigation.view.core.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by roman on 2/13/17.
 */

public class MaxEmittersFragment extends BaseFragment {

    @BindView(R.id.f_max_emitters_flow_rate_spinner)
    Spinner mFlowRateSpinner;
    @BindView(R.id.f_max_emitters_number_of_emitters_input)
    EditText mNumberOfEmittersInputField;
    @BindView(R.id.f_max_emitters_ips_spinner)
    Spinner mIpsSpinner;
    @BindView(R.id.f_max_emitters_pe_spinner)
    Spinner mPeSpinner;
    @BindView(R.id.f_max_emitters_ips_result)
    TextView mIPSResult;
    @BindView(R.id.f_max_emitters_pe_result)
    TextView mPEResult;

    private List<SizeOfHose> mIPSSizesOfHose;
    private List<SizeOfHose> mPESizesOfHose;

    private MaxEmittersInput mInput;

    public static MaxEmittersFragment newInstance() {
        Bundle args = new Bundle();
        MaxEmittersFragment fragment = new MaxEmittersFragment();
        fragment.setArguments(args);
        return fragment;
    }

    //region BaseFragment
    @Override
    protected int getContentViewID() {
        return R.layout.fragment_max_emitters;
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        ButterKnife.bind(this, view);
        initSpinners();
        startInputInit();
        initMaxEmittersInput();
    }
    //endregion

    //region Utility API
    private void initSpinners() {
        initHoseValues();

        final String[] flowRates = getResources().getStringArray(R.array.choose_flow_rate);
        ArrayAdapter flowRateAdapter = new ArrayAdapter<>(getContext(), R.layout.base_spinner_drop_down, flowRates);
        flowRateAdapter.setDropDownViewResource(R.layout.base_spinner_drop_down);
        mFlowRateSpinner.setAdapter(flowRateAdapter);
        mFlowRateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mInput.setFlowRate(mFlowRateSpinner.getSelectedItem().toString());
                setCalculationResult();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final String[] ipsValues = getResources().getStringArray(R.array.ips);
        ArrayAdapter ipsAdapter = new ArrayAdapter<>(getContext(), R.layout.base_spinner_drop_down, ipsValues);
        ipsAdapter.setDropDownViewResource(R.layout.base_spinner_drop_down);
        mIpsSpinner.setAdapter(ipsAdapter);
        mIpsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mInput.setSizeOfHoseIPS(getSelectedHose(mIPSSizesOfHose, mIpsSpinner.getSelectedItem().toString()));
                setCalculationResult();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final String[] peValues = getResources().getStringArray(R.array.pe);
        ArrayAdapter peAdapter = new ArrayAdapter<>(getContext(), R.layout.base_spinner_drop_down, peValues);
        peAdapter.setDropDownViewResource(R.layout.base_spinner_drop_down);
        mPeSpinner.setAdapter(peAdapter);
        mPeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mInput.setSizeOfHosePE(getSelectedHose(mPESizesOfHose, mPeSpinner.getSelectedItem().toString()));
                setCalculationResult();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void startInputInit() {
        mInput = new MaxEmittersInput();
        mInput.setFlowRate(mFlowRateSpinner.getSelectedItem().toString());
        mInput.setSizeOfHoseIPS(getSelectedHose(mIPSSizesOfHose, mIpsSpinner.getSelectedItem().toString()));
        mInput.setSizeOfHosePE(getSelectedHose(mPESizesOfHose, mPeSpinner.getSelectedItem().toString()));
    }

    private void setCalculationResult() {
        if (!TextUtils.isEmpty(mNumberOfEmittersInputField.getText().toString()) &&
                !mNumberOfEmittersInputField.getText().toString().equals(".") &&
                Double.valueOf(mNumberOfEmittersInputField.getText().toString()) != 0) {
            mIPSResult.setText(String.valueOf(Calculator.calculateMaxEmitters(mInput, true)));
            mPEResult.setText(String.valueOf(Calculator.calculateMaxEmitters(mInput, false)));
        }
        else {
            mIPSResult.setText("0");
            mPEResult.setText("0");
        }
    }

    private void initHoseValues() {
        String[] ipsHose = getResources().getStringArray(R.array.ips);
        mIPSSizesOfHose = new ArrayList<>();
        mIPSSizesOfHose.add(new SizeOfHose(ipsHose[0], 0.5d));
        mIPSSizesOfHose.add(new SizeOfHose(ipsHose[1], 0.56d));
        mIPSSizesOfHose.add(new SizeOfHose(ipsHose[2], 0.75d));
        mIPSSizesOfHose.add(new SizeOfHose(ipsHose[3], 1d));

        String[] peHose = getResources().getStringArray(R.array.pe);
        mPESizesOfHose = new ArrayList<>();
        mPESizesOfHose.add(new SizeOfHose(peHose[0], 0.6d));
        mPESizesOfHose.add(new SizeOfHose(peHose[1], 0.62d));
        mPESizesOfHose.add(new SizeOfHose(peHose[2], 0.82d));
        mPESizesOfHose.add(new SizeOfHose(peHose[3], 1.06d));
    }

    private SizeOfHose getSelectedHose(List<SizeOfHose> list, String value) {
        for (SizeOfHose hose : list) {
            if (value.equals(hose.getTitle()))
                return hose;
        }
        return null;
    }

    private void initMaxEmittersInput() {
        mNumberOfEmittersInputField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(final Editable editable) {
                if (!TextUtils.isEmpty(editable.toString())) {
                    if (!editable.toString().equals(".") && Double.valueOf(editable.toString()) != 0) {
                        mInput.setLengthOfRun(editable.toString());
                        setCalculationResult();
                    } else {
                        mIPSResult.setText("0");
                        mPEResult.setText("0");
                    }
                }
            }
        });
    }
    //endregion
}
