package com.inoxoft.gph.gphirrigation.view.core;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.inoxoft.gph.gphirrigation.GPHApp;
import com.inoxoft.gph.gphirrigation.view.listeners.ComponentCommunicator;

/**
 * Created by binary on 11/26/15.
 */
public abstract class BaseFragment extends Fragment implements android.os.Handler.Callback {

    private static final int MSG_SHOW_LOADING_DIALOG = 0x1000;
    private static final int MSG_UPDATE_LOADING_MESSAGE = 0x1001;
    private static final int MSG_HIDE_LOADING_DIALOG = 0x1002;

    protected final android.os.Handler mUIHandler = new android.os.Handler(BaseFragment.this);
    private ProgressDialog mProgressDialog;
    protected GPHApp mApp;
    protected ComponentCommunicator mComponentCommunicator;

    public BaseFragment() {
        setHasOptionsMenu(true);
    }

    //region Fragment
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mApp = (GPHApp) activity.getApplication();
        if (activity instanceof ComponentCommunicator) {
            mComponentCommunicator = (ComponentCommunicator) activity;
        } else {
            throw new ClassCastException(activity.toString() + " must implement ComponentCommunicator");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(getContentViewID(), container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view, savedInstanceState);
    }
    //endregion

    //region BaseFragment

    /**
     * Returns content view id.
     *
     * @return content id
     */
    protected abstract int getContentViewID();

    /**
     * Init's view.
     *
     * @param savedInstanceState instance of {@link Bundle}
     */
    protected abstract void initView(View view, Bundle savedInstanceState);

    public void removeFragment(String tag) {
        FragmentManager manager = getFragmentManager();
        if (manager == null) {
            return;
        }
        Fragment hideFragment = manager.findFragmentByTag(tag);
        if (hideFragment != null) {
            FragmentTransaction tr = manager.beginTransaction();
            tr.remove(hideFragment);
            tr.commit();
        }
        manager.popBackStack();
    }

    public void replaceFragment(int containerViewId, Fragment fragment, boolean addToBackStack) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(containerViewId, fragment, fragment.getClass().getName());
        if (addToBackStack) {
            transaction.addToBackStack(fragment.getClass().getName());
        }

        transaction.commit();
    }

    public void addFragment(int containerViewId, Fragment fragment, boolean addToBackStack) {
        String tag = fragment.getClass().getName();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(containerViewId, fragment, tag);
        if (addToBackStack) {
            transaction.addToBackStack(tag);
        }
        transaction.commit();
    }

    public void addFragment(int containerViewId, Fragment fragment, boolean addToBackStack, String tag) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(containerViewId, fragment, fragment.getClass().getName());
        if (addToBackStack) {
            transaction.addToBackStack(tag);
        }
        transaction.commit();
    }

    /**
     * Shows loading dialog.
     *
     * @param messageValue message value
     */
    public void showLoadingDialog(String messageValue) {
        Message message = new Message();
        message.what = MSG_SHOW_LOADING_DIALOG;
        message.obj = messageValue;
        mUIHandler.sendMessage(message);
    }

    /**
     * Updates loading dialog message.
     *
     * @param messageValue message value
     */
    public void updateLoadingDialogMessage(String messageValue) {
        Message message = new Message();
        message.what = MSG_UPDATE_LOADING_MESSAGE;
        message.obj = messageValue;
        mUIHandler.sendMessage(message);
    }

    /**
     * Hides loading dialog.
     */
    public void hideLoadingDialog() {
        Message message = new Message();
        message.what = MSG_HIDE_LOADING_DIALOG;
        mUIHandler.sendMessage(message);
    }

    /**
     * Show an error message
     *
     * @param title   A string representing an error title.
     * @param message A string representing an error.
     */
    public void showError(String title, String message) {
        new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create().show();
    }

    public void showError(String title, String message, DialogInterface.OnClickListener listener) {
        new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", listener).create().show();
    }
    //endregion

    //region Handler.Callback
    @Override
    public boolean handleMessage(Message message) {
        boolean result = false;
        switch (message.what) {
            case MSG_SHOW_LOADING_DIALOG:
                if (null == mProgressDialog) {
                    mProgressDialog = new ProgressDialog(getActivity());
                    mProgressDialog.setCancelable(false);
                    mProgressDialog.setCanceledOnTouchOutside(false);
                }
                mProgressDialog.setMessage((String) message.obj);
                if (!mProgressDialog.isShowing()) {
                    mProgressDialog.show();
                }
                result = true;
                break;
            case MSG_UPDATE_LOADING_MESSAGE:
                if (null != mProgressDialog) {
                    mProgressDialog.setMessage((String) message.obj);
                }
                result = true;
                break;
            case MSG_HIDE_LOADING_DIALOG:
                if ((null != mProgressDialog) && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
                result = true;
                break;
        }
        return result;
    }
    //endregion
}
