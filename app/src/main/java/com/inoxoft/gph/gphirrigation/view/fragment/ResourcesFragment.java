package com.inoxoft.gph.gphirrigation.view.fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.inoxoft.gph.gphirrigation.R;
import com.inoxoft.gph.gphirrigation.view.core.BaseFragment;
import com.inoxoft.gph.gphirrigation.view.listeners.MainActivityComponentProvider;
import com.inoxoft.gph.gphirrigation.view.listeners.OnSingleClickListener;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by roman on 2/13/17.
 */

public class ResourcesFragment extends BaseFragment {

    @BindView(R.id.resources_toolbar)
    Toolbar mToolbar;
    @BindView(R.id.website_card)
    CardView mWebSiteCard;
    @BindView(R.id.youtube_card)
    CardView mYouTubeCard;
    @BindView(R.id.catalog_card)
    CardView mCatalogCard;

    private MainActivityComponentProvider mMainActivityComponentProvider;

    public static ResourcesFragment newInstance() {
        Bundle args = new Bundle();
        ResourcesFragment fragment = new ResourcesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    //region BaseFragment
    @Override
    protected int getContentViewID() {
        return R.layout.fragment_resources;
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        ButterKnife.bind(this, view);
        initToolbar();
        mMainActivityComponentProvider.setStatusBarColor(R.color.colorPrimaryDark);
        initCardViews();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MainActivityComponentProvider) {
            mMainActivityComponentProvider = (MainActivityComponentProvider) activity;
        } else {
            throw new ClassCastException(activity.toString() + " must implement MainActivityComponentProvider");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            getActivity().onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }
    //endregion

    //region Utility API
    private void initToolbar() {
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    private void initCardViews() {
        mWebSiteCard.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                openURI(getString(R.string.website_url));
            }
        });

        mYouTubeCard.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                openURI(getString(R.string.youtube_url));
            }
        });

        mCatalogCard.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View view) {
                openURI(getString(R.string.catalog_url));
            }
        });
    }

    private void openURI(String uri) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(uri));
        startActivity(intent);
    }
    //endregion
}
