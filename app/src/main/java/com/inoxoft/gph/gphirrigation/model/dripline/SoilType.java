package com.inoxoft.gph.gphirrigation.model.dripline;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ie-07 on 2/21/17.
 */

public class SoilType implements Parcelable {

    private String mTitle;
    private double mValue;

    public SoilType(String title, double value) {
        mTitle = title;
        mValue = value;
    }

    public String getTitle() {
        return mTitle;
    }

    public double getValue() {
        return mValue;
    }

    //region Parcelable
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mTitle);
        dest.writeDouble(this.mValue);
    }

    protected SoilType(Parcel in) {
        this.mTitle = in.readString();
        this.mValue = in.readDouble();
    }

    public static final Creator<SoilType> CREATOR = new Creator<SoilType>() {
        @Override
        public SoilType createFromParcel(Parcel source) {
            return new SoilType(source);
        }

        @Override
        public SoilType[] newArray(int size) {
            return new SoilType[size];
        }
    };
    //endregion
}
