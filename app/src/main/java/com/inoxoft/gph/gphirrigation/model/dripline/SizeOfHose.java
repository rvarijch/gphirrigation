package com.inoxoft.gph.gphirrigation.model.dripline;

/**
 * Created by ie-07 on 2/22/17.
 */

public class SizeOfHose {

    private String mTitle;
    private double mValue;

    public SizeOfHose(String title, double value) {
        mTitle = title;
        mValue = value;
    }

    public String getTitle() {
        return mTitle;
    }

    public double getValue() {
        return mValue;
    }
}
