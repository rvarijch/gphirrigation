package com.inoxoft.gph.gphirrigation.model.dripline;

/**
 * Created by ie-07 on 2/17/17.
 */

public class MaxEmittersInput {

    private String mFlowRate;
    private String mLengthOfRun;
    private SizeOfHose mSizeOfHoseIPS;
    private SizeOfHose mSizeOfHosePE;

    public String getFlowRate() {
        return mFlowRate;
    }

    public void setFlowRate(String flowRate) {
        mFlowRate = flowRate;
    }

    public String getLengthOfRun() {
        return mLengthOfRun;
    }

    public void setLengthOfRun(String lengthOfRun) {
        mLengthOfRun = lengthOfRun;
    }

    public SizeOfHose getSizeOfHoseIPS() {
        return mSizeOfHoseIPS;
    }

    public void setSizeOfHoseIPS(SizeOfHose sizeOfHoseIPS) {
        mSizeOfHoseIPS = sizeOfHoseIPS;
    }

    public SizeOfHose getSizeOfHosePE() {
        return mSizeOfHosePE;
    }

    public void setSizeOfHosePE(SizeOfHose sizeOfHosePE) {
        mSizeOfHosePE = sizeOfHosePE;
    }
}
