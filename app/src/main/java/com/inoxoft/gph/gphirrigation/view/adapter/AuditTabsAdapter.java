package com.inoxoft.gph.gphirrigation.view.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.inoxoft.gph.gphirrigation.view.fragment.audit.DUFragment;
import com.inoxoft.gph.gphirrigation.view.fragment.audit.PrecipitationRateFragment;

/**
 * Created by roman on 2/13/17.
 */

public class AuditTabsAdapter extends FragmentStatePagerAdapter {

    private int mNumOfTabs;

    public AuditTabsAdapter(FragmentManager fm, int numOfTabs) {
        super(fm);
        mNumOfTabs = numOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return PrecipitationRateFragment.newInstance();
            case 1:
                return DUFragment.newInstance();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
