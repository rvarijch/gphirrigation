package com.inoxoft.gph.gphirrigation.view.listeners;

import android.support.v4.app.Fragment;

/**
 * Created by roman on 2/13/17.
 */

public interface ComponentCommunicator {

    void navigateToFragment(Fragment fragment);
}
