package com.inoxoft.gph.gphirrigation.view.fragment.drip;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.inoxoft.gph.gphirrigation.Constants;
import com.inoxoft.gph.gphirrigation.R;
import com.inoxoft.gph.gphirrigation.calculator.Calculator;
import com.inoxoft.gph.gphirrigation.model.dripline.DriplineInput;
import com.inoxoft.gph.gphirrigation.model.dripline.DriplineResult;
import com.inoxoft.gph.gphirrigation.view.adapter.ResultScreenAdapter;
import com.inoxoft.gph.gphirrigation.view.core.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by roman on 2/13/17.
 */

public class DripLineResultFragment extends BaseFragment {

    @BindView(R.id.f_result_recycler_view)
    RecyclerView mRecyclerView;

    public static DripLineResultFragment newInstance(DriplineInput data) {
        Bundle args = new Bundle();
        args.putParcelable(Constants.KEY_BUNDLE_DRIPLINE_INPUT, data);
        DripLineResultFragment fragment = new DripLineResultFragment();
        fragment.setArguments(args);
        return fragment;
    }

    //region BaseFragment
    @Override
    protected int getContentViewID() {
        return R.layout.fragment_drip_line_result;
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        ButterKnife.bind(this, view);
        initRecyclerView();
    }

    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setHasFixedSize(true);
        ResultScreenAdapter mResultScreenAdapter = new ResultScreenAdapter(getContext());
        mResultScreenAdapter.addData(makeCalculations(getArguments()));
        mRecyclerView.setAdapter(mResultScreenAdapter);
    }

    private List<DriplineResult> makeCalculations(Bundle bundle) {
        if (bundle == null) {
            return new ArrayList<>();
        }

        DriplineInput driplineInput = bundle.getParcelable(Constants.KEY_BUNDLE_DRIPLINE_INPUT);
        List<DriplineResult> results = new ArrayList<>();
        results.add(Calculator.calculateDriplineByGPM(driplineInput, Calculator.DRIPLINE_EMITTER_FLOW_ONE));
        results.add(Calculator.calculateDriplineByGPM(driplineInput, Calculator.DRIPLINE_EMITTER_FLOW_HALF));
        return results;
    }
    //endregion
}
