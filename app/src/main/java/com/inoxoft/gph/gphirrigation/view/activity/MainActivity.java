package com.inoxoft.gph.gphirrigation.view.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Window;
import android.view.WindowManager;

import com.inoxoft.gph.gphirrigation.R;
import com.inoxoft.gph.gphirrigation.view.core.BaseActivity;
import com.inoxoft.gph.gphirrigation.view.fragment.MainFragment;
import com.inoxoft.gph.gphirrigation.view.listeners.MainActivityComponentProvider;

import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements MainActivityComponentProvider {

    private boolean mButtonsWasAnimated;

    //region BaseActivity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        replaceFragment(R.id.main_fragment_container, MainFragment.newInstance(), false, false);
    }

    @Override
    public void onBackPressed() {
        setStatusBarColor(R.color.colorDivider);
        super.onBackPressed();
    }
    //endregion

    //region ComponentCommunicator
    @Override
    public void navigateToFragment(Fragment fragment) {
        replaceFragment(R.id.main_fragment_container, fragment, true, true);
    }

    public void setButtonsWasAnimated(boolean buttonsWasAnimated) {
        mButtonsWasAnimated = buttonsWasAnimated;
    }

    public boolean getButtonsWasAnimated() {
        return mButtonsWasAnimated;
    }

    @Override
    public void setStatusBarColor(int colorRes) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(colorRes));
        }
    }
    //endregion
}
