package com.inoxoft.gph.gphirrigation.view.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.inoxoft.gph.gphirrigation.view.fragment.drip.DripLineFragment;
import com.inoxoft.gph.gphirrigation.view.fragment.drip.LengthOfRunFragment;
import com.inoxoft.gph.gphirrigation.view.fragment.drip.MaxEmittersFragment;

/**
 * Created by roman on 2/13/17.
 */

public class DripTabsAdapter extends FragmentStatePagerAdapter {

    private int mNumOfTabs;

    public DripTabsAdapter(FragmentManager fm, int numOfTabs) {
        super(fm);
        mNumOfTabs = numOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return DripLineFragment.newInstance();
            case 1:
                return LengthOfRunFragment.newInstance();
            case 2:
                return MaxEmittersFragment.newInstance();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
