package com.inoxoft.gph.gphirrigation.view.fragment.drip;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.inoxoft.gph.gphirrigation.R;
import com.inoxoft.gph.gphirrigation.model.dripline.DriplineInput;
import com.inoxoft.gph.gphirrigation.model.dripline.DriplinePSI;
import com.inoxoft.gph.gphirrigation.model.dripline.SoilType;
import com.inoxoft.gph.gphirrigation.view.core.BaseFragment;
import com.inoxoft.gph.gphirrigation.view.dialog.ErrorDialog;
import com.inoxoft.gph.gphirrigation.view.dialog.InfoDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by roman on 2/13/17.
 */

public class DripLineFragment extends BaseFragment {

    @BindView(R.id.f_dripline_area_type_spinner)
    Spinner mAreaTypeSpinner;
    @BindView(R.id.f_dripline_soil_type_spinner)
    Spinner mSoilTypeSpinner;
    @BindView(R.id.f_dripline_psi_spinner)
    Spinner mPsiSpinner;
    @BindView(R.id.f_dripline_area_input)
    EditText mAreaInputField;
    @BindView(R.id.f_dripline_sloped_istall_group)
    RadioGroup mSlopedInstallRadioGroup;

    private String[] mSoilTypeTitles;
    private String[] mPsiTitles;

    private String mSelectedAreaType;
    private String mSelectedSoilType;
    private String mPsiValue;

    private List<DriplinePSI> mDriplinePSIs;
    private List<SoilType> mSoilTypes;

    public static DripLineFragment newInstance() {
        Bundle args = new Bundle();
        DripLineFragment fragment = new DripLineFragment();
        fragment.setArguments(args);
        return fragment;
    }

    //region BaseFragment
    @Override
    protected int getContentViewID() {
        return R.layout.fragment_drip_line;
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        ButterKnife.bind(this, view);
        initSpinners();
        initRadioGroup();
    }
    //endregion

    //region Utility API
    @OnClick(R.id.f_drip_line_calculate_btn)
    void onCalculateClick() {
        if (isAreaValidInput()) {
            DriplineInput input = new DriplineInput();
            input.setSoilType(getSelectedSoilType(mSelectedSoilType));
            input.setPSI(getSelectedDriplinePSI(mPsiValue));
            input.setArea(mAreaInputField.getText().toString());
            input.setAreaType(mSelectedAreaType);
            input.setIsSlopedInstallation(mSlopedInstallRadioGroup.isSelected());
            mComponentCommunicator.navigateToFragment(DripLineResultFragment.newInstance(input));
        }
    }

    private boolean isAreaValidInput(){
        if (TextUtils.isEmpty(mAreaInputField.getText().toString())) {
            Toast.makeText(getContext(), "Fill all fields", Toast.LENGTH_SHORT).show();
            return false;
        } else if(mAreaInputField.getText().toString().equals(".") || Double.valueOf(mAreaInputField.getText().toString()) == 0d){
            InfoDialog.showInfoDialog(getActivity(), R.string.dripline_area_must_be_a_number);
            return false;
        }
        else{
            return true;
        }
    }

    private DriplinePSI getSelectedDriplinePSI(String selectedPsiTitle) {
        for (DriplinePSI psi : mDriplinePSIs) {
            if (psi.getTitle().equals(selectedPsiTitle))
                return psi;
        }
        return null;
    }

    private SoilType getSelectedSoilType(String selectedSoilType) {
        for (SoilType type : mSoilTypes) {
            if (type.getTitle().equals(selectedSoilType))
                return type;
        }
        return null;
    }

    private void initSpinners() {
        initSpinnerObjects();

        final String[] areaTypes = getResources().getStringArray(R.array.area_type);
        ArrayAdapter areaTypeAdapter = new ArrayAdapter<>(getContext(), R.layout.base_spinner_drop_down, areaTypes);
        areaTypeAdapter.setDropDownViewResource(R.layout.base_spinner_drop_down);
        mAreaTypeSpinner.setAdapter(areaTypeAdapter);
        mAreaTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mSelectedAreaType = areaTypes[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter soilTypeAdapter = new ArrayAdapter<>(getContext(), R.layout.base_spinner_drop_down, mSoilTypeTitles);
        soilTypeAdapter.setDropDownViewResource(R.layout.base_spinner_drop_down);
        mSoilTypeSpinner.setAdapter(soilTypeAdapter);
        mSoilTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mSelectedSoilType = mSoilTypeTitles[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter psiAdapter = new ArrayAdapter<>(getContext(), R.layout.base_spinner_drop_down, mPsiTitles);
        psiAdapter.setDropDownViewResource(R.layout.base_spinner_drop_down);
        mPsiSpinner.setAdapter(psiAdapter);
        mPsiSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mPsiValue = mPsiTitles[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initRadioGroup() {
        mSlopedInstallRadioGroup.check(R.id.f_dripline_sloped_install_yes_btn);
        mSlopedInstallRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton checkedButton = (RadioButton) group.findViewById(checkedId);
            }
        });
    }

    private void initSpinnerObjects() {
        mSoilTypeTitles = getResources().getStringArray(R.array.soil_type);
        mPsiTitles = getResources().getStringArray(R.array.psi);

        mDriplinePSIs = new ArrayList<>();
        mDriplinePSIs.add(new DriplinePSI(mPsiTitles[0], 143, 285));
        mDriplinePSIs.add(new DriplinePSI(mPsiTitles[1], 151, 302));
        mDriplinePSIs.add(new DriplinePSI(mPsiTitles[2], 189, 378));
        mDriplinePSIs.add(new DriplinePSI(mPsiTitles[3], 226, 453));
        mDriplinePSIs.add(new DriplinePSI(mPsiTitles[4], 302, 604));

        mSoilTypes = new ArrayList<>();
        mSoilTypes.add(new SoilType(mSoilTypeTitles[0], 0.75f));
        mSoilTypes.add(new SoilType(mSoilTypeTitles[1], 0.5f));
        mSoilTypes.add(new SoilType(mSoilTypeTitles[2], 0.5f));
        mSoilTypes.add(new SoilType(mSoilTypeTitles[3], 0.4f));
        mSoilTypes.add(new SoilType(mSoilTypeTitles[4], 0.3f));
        mSoilTypes.add(new SoilType(mSoilTypeTitles[5], 0.25f));
    }
    //endregion
}
