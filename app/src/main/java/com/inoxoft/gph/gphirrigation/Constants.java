package com.inoxoft.gph.gphirrigation;

/**
 * Created by ie-07 on 2/21/17.
 */

public final class Constants {

    public static final String KEY_BUNDLE_DRIPLINE_INPUT = "key_bundle_dripline_input";
    public static final int BASE_ANIMATION_DURATION = 250;
}
