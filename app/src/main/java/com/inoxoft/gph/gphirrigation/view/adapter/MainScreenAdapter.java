package com.inoxoft.gph.gphirrigation.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.inoxoft.gph.gphirrigation.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by roman on 2/14/17.
 */

public class MainScreenAdapter extends RecyclerView.Adapter<MainScreenAdapter.ViewHolder> {

    private final static int FADE_DURATION = 350;
    private OnItemClickListener mOnItemClickListener;
    private List<ViewItem> mDataList;
    private boolean mWasAnimated;

    public MainScreenAdapter(OnItemClickListener listener) {
        mOnItemClickListener = listener;
        generateDataList();
    }

    //region RecyclerView.Adapter
    @Override
    public MainScreenAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ViewItem item = mDataList.get(position);
        holder.mTitle.setText(item.mTitleRes);
        holder.mIcon.setImageResource(item.mIconRes);
        if (!mWasAnimated) {
            setScaleAnimation(holder.itemView);
        }
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    public void setWasAnimated(boolean wasAnimated) {
        mWasAnimated = wasAnimated;
    }
    //endregion

    //region Utility API
    private void generateDataList() {
        mDataList = new ArrayList<>();
        mDataList.add(new ViewItem(R.drawable.ic_png_dripline, R.string.dripline_calculator));
        mDataList.add(new ViewItem(R.drawable.ic_png_audit, R.string.audit_calculator));
        mDataList.add(new ViewItem(R.drawable.ic_png_resources, R.string.resources));
        mDataList.add(new ViewItem(R.drawable.ic_png_contact, R.string.contact));
    }

    private void setScaleAnimation(View view) {
        ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF,
                0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        anim.setDuration(FADE_DURATION);
        view.startAnimation(anim);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView mIcon;
        TextView mTitle;

        private ViewHolder(View v) {
            super(v);
            mIcon = (ImageView) v.findViewById(R.id.item_main_icon);
            mTitle = (TextView) v.findViewById(R.id.item_main_title);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(getAdapterPosition(), mDataList.get(getAdapterPosition()).mTitleRes);
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {

        void onItemClick(int position, int titleRes);
    }

    private class ViewItem {

        private int mIconRes;
        private int mTitleRes;

        private ViewItem(int iconRes, int title) {
            mIconRes = iconRes;
            mTitleRes = title;
        }
    }
    //endregion
}


